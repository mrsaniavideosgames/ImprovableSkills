package org.zeith.improvableskills.custom.pagelets;

import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.zeith.improvableskills.InfoIS;
import org.zeith.improvableskills.api.PlayerSkillData;
import org.zeith.improvableskills.api.registry.PageletBase;
import org.zeith.improvableskills.client.gui.GuiAbilityBook;

public class PageletAbilities extends PageletBase
{
	{
		setRegistryName(InfoIS.MOD_ID, "abilities");
		setIcon(new ItemStack(Blocks.ENCHANTING_TABLE));
		setTitle(new TextComponentTranslation("pagelet." + InfoIS.MOD_ID + ":abilities"));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public GuiAbilityBook createTab(PlayerSkillData data)
	{
		return new GuiAbilityBook(this, data);
	}
}