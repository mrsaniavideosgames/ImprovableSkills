package org.zeith.improvableskills.custom.pagelets;

import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.zeith.improvableskills.InfoIS;
import org.zeith.improvableskills.api.PlayerSkillData;
import org.zeith.improvableskills.api.registry.PageletBase;
import org.zeith.improvableskills.client.gui.GuiSkillsBook;
import org.zeith.improvableskills.init.SkillsIS;
import org.zeith.improvableskills.items.ItemSkillScroll;

public class PageletSkills extends PageletBase
{
	{
		setRegistryName(InfoIS.MOD_ID, "skills");
		setIcon(ItemSkillScroll.of(SkillsIS.HEALTH));
		setTitle(new TextComponentTranslation("pagelet." + InfoIS.MOD_ID + ":skills"));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public GuiSkillsBook createTab(PlayerSkillData data)
	{
		return new GuiSkillsBook(this, data);
	}
}