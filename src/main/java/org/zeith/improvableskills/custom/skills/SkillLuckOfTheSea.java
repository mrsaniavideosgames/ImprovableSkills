package org.zeith.improvableskills.custom.skills;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityFishHook;
import net.minecraft.world.storage.loot.LootTableList;
import org.zeith.improvableskills.InfoIS;
import org.zeith.improvableskills.api.PlayerSkillData;
import org.zeith.improvableskills.api.registry.PlayerSkillBase;

import java.util.UUID;

public class SkillLuckOfTheSea extends PlayerSkillBase
{
	public static final UUID LOTS_LUCK = UUID.fromString("d489061e-0b53-4aa3-a7f4-f1a9a726ef49");
	
	public SkillLuckOfTheSea()
	{
		super(15);
		setRegistryName(InfoIS.MOD_ID, "luck_of_the_sea");
		
		hasScroll = true;
		genScroll = true;
		
		getLoot().chance.n = 10;
		getLoot().setLootTable(LootTableList.GAMEPLAY_FISHING);
		
		xpCalculator.xpValue = 2;
	}
	
	@Override
	public void tick(PlayerSkillData data)
	{
		EntityPlayer player = data.player;
		EntityFishHook hook = player.fishEntity;
		int level = data.getSkillLevel(this);
		IAttributeInstance luck = player.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.LUCK);
		luck.removeModifier(LOTS_LUCK);
		if((hook != null) && (!hook.isDead))
			luck.applyModifier(new AttributeModifier(LOTS_LUCK, "IS3 Fishing Luck", level * 2D, 0));
	}
}