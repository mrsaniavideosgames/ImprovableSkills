package org.zeith.improvableskills.custom.skills;

import net.minecraft.world.storage.loot.LootTableList;
import org.zeith.improvableskills.InfoIS;
import org.zeith.improvableskills.api.registry.PlayerSkillBase;

public class SkillEnchanter extends PlayerSkillBase
{
	public SkillEnchanter()
	{
		super(20);
		setRegistryName(InfoIS.MOD_ID, "enchanter");
		
		hasScroll = true;
		genScroll = true;
		
		getLoot().chance.n = 4;
		getLoot().setLootTable(LootTableList.CHESTS_STRONGHOLD_LIBRARY);
		
		xpCalculator.xpValue = 2;
	}
}