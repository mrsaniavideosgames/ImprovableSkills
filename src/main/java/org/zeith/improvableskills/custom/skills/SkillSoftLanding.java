package org.zeith.improvableskills.custom.skills;

import org.zeith.improvableskills.InfoIS;
import org.zeith.improvableskills.api.registry.PlayerSkillBase;

public class SkillSoftLanding extends PlayerSkillBase
{
	public SkillSoftLanding()
	{
		super(10);
		setRegistryName(InfoIS.MOD_ID, "soft_landing");
		xpCalculator.xpValue = 2;
	}
}