package org.zeith.improvableskills.custom.skills;

import org.zeith.improvableskills.InfoIS;
import org.zeith.improvableskills.api.PlayerSkillData;
import org.zeith.improvableskills.api.registry.PlayerSkillBase;

public class SkillAttackSpeed extends PlayerSkillBase
{
	public SkillAttackSpeed()
	{
		super(25);
		setRegistryName(InfoIS.MOD_ID, "attack_speed");
		
		xpCalculator.baseFormula = "%lvl%^1.5";
	}
	
	@Override
	public void tick(PlayerSkillData data)
	{
		data.player.ticksSinceLastSwing += Math.sqrt(data.getSkillLevel(this)) / 3;
	}
}