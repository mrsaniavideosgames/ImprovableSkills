package org.zeith.improvableskills.custom.skills;

import org.zeith.improvableskills.InfoIS;
import org.zeith.improvableskills.api.registry.PlayerSkillBase;

public class SkillAtkDmgMelee extends PlayerSkillBase
{
	public SkillAtkDmgMelee()
	{
		super(15);
		setRegistryName(InfoIS.MOD_ID, "atkdmg_melee");
		
		xpCalculator.xpValue = 3;
	}
}