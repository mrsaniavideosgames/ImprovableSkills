package org.zeith.improvableskills.custom.skills;

import net.minecraft.world.storage.loot.LootTableList;
import org.zeith.improvableskills.InfoIS;
import org.zeith.improvableskills.api.registry.PlayerSkillBase;

public class SkillObsidianSkin extends PlayerSkillBase
{
	public SkillObsidianSkin()
	{
		super(20);
		setRegistryName(InfoIS.MOD_ID, "obsidian_skin");
		hasScroll = true;
		genScroll = true;
		
		getLoot().chance.n = 3;
		getLoot().setLootTable(LootTableList.CHESTS_NETHER_BRIDGE);
		
		xpCalculator.xpValue = 2;
	}
}