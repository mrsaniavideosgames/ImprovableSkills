package org.zeith.improvableskills.init;

import org.zeith.improvableskills.items.*;

public class ItemsIS
{
	public static final ItemSkillsBook SKILLS_BOOK = new ItemSkillsBook();
	public static final ItemSkillScroll SKILL_SCROLL = new ItemSkillScroll();
	public static final ItemAbilityScroll ABILITY_SCROLL = new ItemAbilityScroll();
	public static final ItemSkillScrollCreative SCROLL_CREATIVE = new ItemSkillScrollCreative();
	public static final ItemParchmentFragment PARCHMENT_FRAGMENT = new ItemParchmentFragment();
}