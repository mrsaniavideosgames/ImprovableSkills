package org.zeith.improvableskills.init;

import net.minecraftforge.registries.IForgeRegistry;
import org.zeith.improvableskills.api.registry.PlayerAbilityBase;
import org.zeith.improvableskills.custom.abilities.PlayerAbilityAnvil;
import org.zeith.improvableskills.custom.abilities.PlayerAbilityCrafter;
import org.zeith.improvableskills.custom.abilities.PlayerAbilityEnchanting;

public class AbilitiesIS
{
	public static final PlayerAbilityEnchanting ENCHANTING = new PlayerAbilityEnchanting();
	public static final PlayerAbilityCrafter CRAFTER = new PlayerAbilityCrafter();
	public static final PlayerAbilityAnvil ANVIL = new PlayerAbilityAnvil();
	public static IForgeRegistry<PlayerAbilityBase> registry;
	
	public static void register(IForgeRegistry<PlayerAbilityBase> reg)
	{
		registry = reg;
		
		reg.register(ENCHANTING);
		reg.register(CRAFTER);
		reg.register(ANVIL);
	}
}