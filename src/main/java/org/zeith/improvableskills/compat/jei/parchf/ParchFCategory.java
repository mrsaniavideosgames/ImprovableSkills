package org.zeith.improvableskills.compat.jei.parchf;

import com.zeitheron.hammercore.utils.ConsumableItem;
import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.gui.IDrawable;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeCategory;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.zeith.improvableskills.InfoIS;
import org.zeith.improvableskills.compat.jei.JeiIS3;
import org.zeith.improvableskills.init.ItemsIS;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class ParchFCategory implements IRecipeCategory<ParchFWrapper>
{
	public IDrawable bg;
	
	public ParchFCategory(IJeiHelpers helper)
	{
		this.bg = helper.getGuiHelper().createDrawable(new ResourceLocation(InfoIS.MOD_ID, "textures/gui/jei.png"), 0, 0, 132, 34);
	}
	
	@Override
	public String getUid()
	{
		return JeiIS3.PARCHF_CAT;
	}
	
	@Override
	public String getTitle()
	{
		return I18n.format("jei." + InfoIS.MOD_ID + ":parchf");
	}
	
	@Override
	public String getModName()
	{
		return InfoIS.MOD_NAME;
	}
	
	@Override
	public IDrawable getBackground()
	{
		return bg;
	}
	
	@Nullable
	@Override
	public IDrawable getIcon()
	{
		return null;
	}
	
	@Override
	public void setRecipe(IRecipeLayout recipeLayout, ParchFWrapper recipeWrapper, IIngredients ingredients)
	{
		IGuiItemStackGroup items = recipeLayout.getItemStacks();
		
		items.init(0, true, 7, 8);
		items.set(0, new ItemStack(ItemsIS.PARCHMENT_FRAGMENT));
		
		items.init(1, false, 107, 8);
		items.set(1, recipeWrapper.recipe.output.copy());
		
		int j = 0;
		for(ConsumableItem ci : recipeWrapper.recipe.itemsIn)
		{
			items.init(j + 2, true, j * 18 + 25, 8);
			items.set(j + 2, ci2stacks(ci));
			++j;
		}
	}
	
	public static List<ItemStack> ci2stacks(ConsumableItem ci)
	{
		List<ItemStack> stacks = new ArrayList<>();
		for(ItemStack stack : ci.ingr.getMatchingStacks())
		{
			ItemStack s = stack.copy();
			s.setCount(s.getCount() * ci.amount);
			stacks.add(s);
		}
		return stacks;
	}
}
