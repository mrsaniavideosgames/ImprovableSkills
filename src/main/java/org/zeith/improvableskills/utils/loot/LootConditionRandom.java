package org.zeith.improvableskills.utils.loot;

import net.minecraft.world.storage.loot.LootContext;
import net.minecraft.world.storage.loot.conditions.LootCondition;
import org.zeith.improvableskills.api.loot.RandomBoolean;

import java.util.Random;

public class LootConditionRandom implements LootCondition
{
	public RandomBoolean oneInN;
	
	public LootConditionRandom(RandomBoolean oneInN)
	{
		this.oneInN = oneInN;
	}
	
	@Override
	public boolean testCondition(Random rand, LootContext context)
	{
		if(!oneInN.get(rand))
			return false;
		return true;
	}
}