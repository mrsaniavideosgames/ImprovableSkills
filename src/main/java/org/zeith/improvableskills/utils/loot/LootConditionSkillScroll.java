package org.zeith.improvableskills.utils.loot;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.storage.loot.LootContext;
import net.minecraft.world.storage.loot.conditions.LootCondition;
import org.zeith.improvableskills.api.loot.RandomBoolean;
import org.zeith.improvableskills.api.registry.PlayerSkillBase;
import org.zeith.improvableskills.data.PlayerDataManager;

import java.util.Random;

public class LootConditionSkillScroll implements LootCondition
{
	public RandomBoolean oneInN;
	private final String id;
	
	public LootConditionSkillScroll(PlayerSkillBase used, RandomBoolean oneInN)
	{
		id = used.getRegistryName().toString();
		this.oneInN = oneInN;
	}
	
	@Override
	public boolean testCondition(Random rand, LootContext context)
	{
		if(!oneInN.get(rand))
			return false;
		Entity ent = context.getKillerPlayer();
		if(ent instanceof EntityPlayer)
			return !PlayerDataManager.handleDataSafely((EntityPlayer) ent, data -> data.stat_scrolls.contains(id), false).booleanValue();
		return false;
	}
}