package org.zeith.improvableskills.proxy;

import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.zeith.improvableskills.api.PlayerSkillData;

public class SyncSkills
{
	public static PlayerSkillData CLIENT_DATA;
	
	@SideOnly(Side.CLIENT)
	public static PlayerSkillData getData()
	{
		if(CLIENT_DATA == null || CLIENT_DATA.player != Minecraft.getMinecraft().player)
			return CLIENT_DATA = new PlayerSkillData(Minecraft.getMinecraft().player);
		return CLIENT_DATA;
	}
}