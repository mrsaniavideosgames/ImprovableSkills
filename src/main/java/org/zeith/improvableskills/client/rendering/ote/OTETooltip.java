package org.zeith.improvableskills.client.rendering.ote;

import org.lwjgl.opengl.GL11;
import org.zeith.improvableskills.client.rendering.OTEffect;
import org.zeith.improvableskills.client.rendering.OnTopEffects;

import java.util.ArrayList;
import java.util.List;

public class OTETooltip extends OTEffect
{
	public final List<String> tooltip = new ArrayList<>();
	private int time;
	private static OTETooltip cinst;
	
	public static void showTooltip(String... tip)
	{
		if(cinst == null)
			cinst = new OTETooltip();
		cinst.tooltip.clear();
		for(String t : tip)
			cinst.tooltip.add(t);
		
		/** Always on top */
		if(OnTopEffects.effects.indexOf(cinst) != OnTopEffects.effects.size() - 1)
		{
			OnTopEffects.effects.remove(cinst);
			OnTopEffects.effects.add(cinst);
		}
	}
	
	public static void showTooltip(List<String> tip)
	{
		if(cinst == null)
			cinst = new OTETooltip();
		cinst.tooltip.clear();
		cinst.tooltip.addAll(tip);
		
		/** Always on top */
		if(OnTopEffects.effects.indexOf(cinst) != OnTopEffects.effects.size() - 1)
		{
			OnTopEffects.effects.remove(cinst);
			OnTopEffects.effects.add(cinst);
		}
	}
	
	{
		OnTopEffects.effects.add(this);
	}
	
	@Override
	public void update()
	{
		if(time++ >= 8)
			setExpired();
		else
		{
			cinst = this;
			
			/** Always on top */
			if(OnTopEffects.effects.indexOf(this) != OnTopEffects.effects.size() - 1)
			{
				OnTopEffects.effects.remove(this);
				OnTopEffects.effects.add(this);
			}
		}
	}
	
	@Override
	public void setExpired()
	{
		super.setExpired();
		cinst = null;
	}
	
	@Override
	public void render(float partialTime)
	{
		if(!tooltip.isEmpty())
		{
			GL11.glPushMatrix();
			GL11.glTranslated(0, 0, 200);
			currentGui.drawHoveringText(tooltip, mouseX, mouseY);
			GL11.glPopMatrix();
			tooltip.clear();
		}
	}
}